function fPromedio(){
    let parrafo = document.getElementById('promedio')
    parrafo.innerHTML = "";
    let arreglo = []
    min = Math.ceil(1);
    max = Math.floor(20)
    let random = Math.floor(Math.random() * (20-1+1)+1);
    for (let con = 0; con < random; con++) {
        
        arreglo.push(Math.floor(Math.random() * (20-1+1)+1));

    }

    var suma = 0, promedio, i;
    for (i = 0; i < arreglo.length; i++) {
        suma = suma + arreglo[i];
    }
    promedio = suma / arreglo.length;
    console.log(promedio)

    parrafo.innerHTML = parrafo.innerHTML + promedio + " es la cantidad promedio del siguiente arreglo: {" + arreglo + "}";
}

function fPares(){
    let parrafo = document.getElementById('pares1');
    parrafo.innerHTML = "";
    let arreglo = []
    min = Math.ceil(1);
    max = Math.floor(20)
    let random = Math.floor(Math.random() * (20-1+1)+1);
    for (let con = 0; con < random; con++) {
        
        arreglo.push(Math.floor(Math.random() * (20-1+1)+1));

    }
    var pares = 0;
    for (let i = 0; i < arreglo.length; i++) {
        if ((arreglo[i] % 2) == 0) {
            pares = pares + 1;            
        }
              
    }
    console.log(pares);

    parrafo.innerHTML = parrafo.innerHTML + pares + " es la cantidad de numeros pares que existen en el siguiente arreglo: {" + arreglo + "}";
}

function fOrdenado(){
    let parrafo = document.getElementById('ordenado')
    parrafo.innerHTML = "";
    
    let arreglo = []
    min = Math.ceil(1);
    max = Math.floor(20)
    let random = Math.floor(Math.random() * (20-1+1)+1);
    let arreglo2 = [];
    for (let con = 0; con < random; con++) {
        
        arreglo.push(Math.floor(Math.random() * (20-1+1)+1));
        arreglo2.push(arreglo[con]);
        
    }

    

    var i, k, aux;
    for (k = 1; k < arreglo.length; k++) {
        for (i = 0; i < (arreglo.length - k); i++) {
            if (arreglo[i] < arreglo[i + 1]) {
                aux = arreglo[i];
                arreglo[i] = arreglo[i + 1];
                arreglo[i + 1] = aux;
            }
        }
    }

    
    parrafo.innerHTML = parrafo.innerHTML + arreglo + " es arreglo ordenado de mayor a menor conseguido del siguiente arreglo: {" + arreglo2 + "}";
}

// Clase 24/10/2022
// Generación de objetos
function genera(){
    let limite = document.getElementById('limite').value;
    let lstNumeros = document.getElementById('idNumeros');
    lstNumeros.innerHTML = "";


    for (let con = 0; con < limite; con++) {
        
        lstNumeros.options[con] = new Option(Math.floor(Math.random() * (limite-1+1)+1), 'texto', Math.floor(Math.random() * (limite-1+1)+1));
        
        // lstNumeros.push(Math.floor(Math.random() * (50-1+1)+1));
        
    }
}


function comprobar(){
    let borrarn = document.getElementById('cantidad');
    borrarn.innerHTML = "";
    
    let lstNumeros = []
    min = Math.ceil(1);
    max = Math.floor(50);
    let random = Math.floor(Math.random() * (50-1+1)+1);
    console.log("Hay: " + random + " de cantidad");
    let n = document.getElementById('cantidad');
    n.innerHTML = n.innerHTML + "El rango del numero es: "+ random;
    let borrar1 = document.getElementById('pares');
    borrar1.innerHTML = "";
    let borrar2 = document.getElementById('impares');
    borrar2.innerHTML = "";
    let borrar3 = document.getElementById('simetrico');
    borrar3.innerHTML = "";


    var pares = 0;
    var impares = 0;

    for (let con = 0; con < random; con++) {
        
        lstNumeros.push(Math.floor(Math.random() * (50-1+1)+1));

    }

    arregloRandom = document.getElementById('arregloRandom');
    arregloRandom.innerHTML = "";
    for (let con = 0; con < random; con++) {
        
        arregloRandom.innerHTML = arregloRandom.innerHTML + lstNumeros[con] + " , ";
        
    }
    
    lstNumeros = [];

    for (let con = 1; con < random+1; con++) {
        
        if ((con % 2) == 0) {
            pares = pares + 1;         
        }else{
            impares = impares + 1;
        }

    }

    console.log("Pares hay: " + pares);
    console.log("Impares hay: " + impares)

    var porcPar = (pares / random) * 100;
    var porcImpar = (impares / random) * 100;
    
    let p1 = document.getElementById('pares');
    p1.innerHTML = p1.innerHTML + "Hay un " + porcPar + "% de numeros pares";
    let p2 = document.getElementById('impares');
    p2.innerHTML = p2.innerHTML + "Hay un " + porcImpar + "% de numeros impares";
    
    if(porcPar > 25 && porcImpar > 25){
        let p3 = document.getElementById('simetrico');
        p3.innerHTML = p3.innerHTML + "El numero es simetrico";
    }else if(porcPar < 25 || porcImpar < 25){
        let p3 = document.getElementById('simetrico');
        p3.innerHTML = p3.innerHTML + "El numero no es simetrico";

    }

}